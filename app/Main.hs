module Main where

import           Caesar
import           Text.Printf
import           Vigenere

caesarExample :: Int -> String -> IO ()
caesarExample shift text =
  let encryptedText = caesar shift text
      decryptedText = unCaesar shift encryptedText
  in
    do
      printf "caesar \"%v\" %v == \"%v\"\n" text shift encryptedText
      printf "unCaesar \"%v\" %v == \"%v\"\n" encryptedText shift decryptedText

vigenereExample :: String -> String -> IO ()
vigenereExample key text =
  let encryptedText = vigenere key text
      decryptedText = unVigenere key encryptedText
  in
    do
      printf "vigenere \"%v\" \"%v\" == \"%v\"\n" key text encryptedText
      printf "unVigenere \"%v\" \"%v\" == \"%v\"\n" key encryptedText decryptedText

main :: IO ()
main = do
  caesarExample 7 "ATTACKATDAWN"
  vigenereExample "LEMON" "ATTACKATDAWN"
