module Caesar
  (
    caesar,
    unCaesar
  ) where

import           Data.Char

caesar :: Int -> String -> String
caesar i s = map (chr . (+ a) . (`mod` 26) . (+ i) . subtract a . ord) s
  where a = ord 'A'

unCaesar :: Int -> String -> String
unCaesar i s = map (chr . (+ a) . (`mod` 26) . subtract i . subtract a . ord) s
  where a = ord 'A'
