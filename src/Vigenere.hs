module Vigenere
  (
    vigenere,
    unVigenere
  )
where

-- | Vigenère cipher.
--
-- https://en.wikipedia.org/wiki/Vigen%C3%A8re_cipher
--
-- The Vigenère cipher is a method of encrypting alphabetic text
-- by using a series of different Caesar ciphers based on the letters
-- of a keyword. It is a simple form of polyalphabetic substitution.
--
-- In a Caesar cipher, each letter of the alphabet is shifted along
-- some number of places; for example, in a Caesar cipher of shift 3,
-- 'A' would become 'D', 'B' would become 'E', 'Y' would become 'B'
-- and so on.
--
-- The Vigenère cipher consists of several Caesar ciphers in sequence
-- with different shift values. The substitution for each letter is
-- determined by a fixed keyword.
--
-- For example, if you want to encode the message "ATTACKATDAWN",
-- the first step is to pick a keyword, that will determine which
-- Caesar cipher to use. We will use keyword "LEMON" here.
--
-- You repeat the keyword for as many characters as there are in
-- your original message:
--
--     ATTACKATDAWN
--     LEMONLEMONLE
--
-- Now, the number of rightward shifts to make to encode each
-- character is set by the character of the keyword that lines up
-- with it. The 'L' means a shift of 11, so the the initial 'A'
-- becomes 'A' + 11 = 'L'. 'E' means a shift of 4, so the 'T'
-- becomes 'T' + 4 = 'X'm and so on:
--
--     ATTACKATDAWN
--     LEMONLEMONLE
--     LXFOPVEFRNHR
--

import           Data.Char


encrypt :: (Int -> Int -> Int) -> String -> String -> String
encrypt applyShift key text =
  map (\(shift, char) -> shift char) $ zip shifts text
  where ordA      = ord 'A'
        shifts    = map (caesar . subtract ordA . ord) $ repeatKey
        repeatKey = concat $ take (n + 1) $ repeat key
          where n = length text `div` length key
        caesar :: Int -> Char -> Char
        caesar shift char =
          chr $ (applyShift (ord char - ordA) shift) `mod` 26 + ordA

vigenere :: String -> String -> String
vigenere = encrypt (+)


unVigenere :: String -> String -> String
unVigenere = encrypt (-)
